const express = require('express');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const app = express();

app.use(express.static('data'));

app.get('/', function (req, res) {
  res.sendFile('data/start.html' , { root : __dirname});
});

const recipeRouter = require('./router/recipeRouter');
app.use('/recipes', recipeRouter);

const storeRouter = require('./router/storeRouter');
app.use('/stores', storeRouter);

const loginRouter = require('./router/loginRouter');
app.use('/login', loginRouter);

app.listen(3000, function () {
    console.log('listening on port 3000!');
  });
  