const express = require('express');
const app = express.Router();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const mysql = require('mysql');
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'nodemysql'
})
db.connect((err) => {
    if (err){
      throw err
    }
    console.log('StoreRouter connected...')
  })
  app.get('/Alle', jsonParser, (req, res) => {
    let sql = `SELECT storeranges.StoreID, products.ProductName, storeranges.Price, stores.StoreName FROM storeranges INNER JOIN products ON storeranges.ProductID = products.ProductID INNER JOIN stores ON storeranges.StoreID = stores.StoreID`;
    let id;
    let query = db.query(sql, (err, result) => {
        if (err) {
            throw err;
        }
        else {
            console.log(result);
            res.json(result)
        }
        
    })
  })


app.get('/Edoka', jsonParser, (req, res) => {
    let sql = `SELECT storeranges.StoreID, products.ProductName, storeranges.Price, stores.StoreName FROM storeranges INNER JOIN products ON storeranges.ProductID = products.ProductID INNER JOIN stores ON storeranges.StoreID = stores.StoreID WHERE stores.StoreID = 1`;
    let id;
    let query = db.query(sql, (err, result) => {
        if (err) {
            throw err;
        }
        else {
            console.log(result);
            res.json(result)
        }
        
    })
  })
  
  app.get('/Brutto', jsonParser, (req, res) => {
    let sql = `SELECT storeranges.StoreID, products.ProductName, storeranges.Price, stores.StoreName FROM storeranges INNER JOIN products ON storeranges.ProductID = products.ProductID INNER JOIN stores ON storeranges.StoreID = stores.StoreID WHERE stores.StoreID = 2`;
    let query = db.query(sql, (err, result) => {
        if (err) {
            throw err;
        }
        else {
            console.log(result);
            res.json(result)
        }
        
    })
  })
  
  app.get('/Were', jsonParser, (req, res) => {
    let sql = `SELECT storeranges.StoreID, products.ProductName, storeranges.Price, stores.StoreName FROM storeranges INNER JOIN products ON storeranges.ProductID = products.ProductID INNER JOIN stores ON storeranges.StoreID = stores.StoreID WHERE stores.StoreID = 3`;
    let query = db.query(sql, (err, result) => {
        if (err) {
            throw err;
        }
        else {
            console.log(result);
            res.json(result)
        }
        
    })
  })
  

module.exports = app;