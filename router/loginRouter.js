const express = require('express');
const app = express.Router();
const bcrypt = require('bcrypt')
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const mysql = require('mysql');
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'nodemysql'
})
db.connect((err) => {
    if (err){
      throw err
    }
    console.log('LoginRouter connected...')
})


app.post('/signup', jsonParser, (req, res) => {
  let array = [];
  let Username;
  let Vorname;
  let Password;
  const member = req.body;
  for (key in member) {
    if (key === "username") {
      Username = member[key]
    }
    else if (key === "vorname") {
      Vorname = member[key]
    }
    else if (key === "nachname") {
      Nachname = member[key]
    }
    else {
      Password = member[key]
    }

  }
   array.push(Username)
   array.push(Vorname)
   array.push(Nachname)

    let sql = `INSERT INTO Users (Username, Vorname, Nachname) VALUES (?,?,?)`;
    let query = db.query(sql, array,(err, result) => {
        if (err) {
            throw err;
        }
        else { 
          while (!result) {}
          
    }
    bcrypt.hash(Password, 10, (err, hash) => {
      if (err) {
        throw err;
      }
      else  {
        array = [];
        Password = hash;
        array.push(Password)
        array.push(Username)
        sql = `INSERT INTO Passwords (Password, UserID) VALUES (?, (Select UserID FROM Users WHERE Username = ?))`;
  
        query = db.query(sql, array,(err, result) => {
            if (err) {
                throw err;
            }
            else {
                res.send('true')
            }
        })
      }
    })
  })
})

app.post('/login', jsonParser, (req, res) => {
  let checkResult;
  let Password;
  let PasswordToCheck;
  let NameToCheck;
  const member = req.body;
  for (key in member) {
    if (key === "name") {
      NameToCheck = member[key]
    }
    else {
      PasswordToCheck = member[key]
    }
  }
  let sql = `SELECT Passwords.PasswordID, Passwords.Password, Users.Username FROM Passwords INNER JOIN Users ON Passwords.UserID = Users.UserID Where Users.Username = ?`;
  let query = db.query(sql, NameToCheck,(err, result) => {
      if (err) {
          throw err;
      }
      else { 
        if (result.length === 0) {
          res.end('false')
        }
        else {
        result.forEach((obj) => {
          for (key in obj) {
            if (key === "Password") {
               Password = obj[key]
            }
          }
        })
      
        bcrypt.compare(PasswordToCheck, Password).then(result => {
          checkResult = result
          if (checkResult == true) {
            res.send('true') 
          }
          else {
            res.send('false')
          }
        })
      }
           err => {
            throw err;
          }
          
        }
      }) 
      
         
  })

  app.post('/signup/checkName', jsonParser, (req, res) => {
    const member = req.body;
  for (key in member) {
    if (key === "name") {
      Name = member[key]
    }
    else {
      Password = member[key]
    }
  }
  sql = `SELECT * FROM Users WHERE Username = ?`;
  
        query = db.query(sql, Name,(err, result) => {
          let message;
            if (err) {
                throw err;
            }
            else {
              console.log(result.length)
              if (result.length == 0) {
               res.send('valid Username')

              }
              else {
              
                
                message = 
                res.send("Username already exists")
              }
                
            }
          })
          
  })

  module.exports = app;