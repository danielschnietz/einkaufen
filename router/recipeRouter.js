const express = require('express');
const app = express.Router();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const mysql = require('mysql');
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'nodemysql'
})
db.connect((err) => {
    if (err){
      throw err
    }
    console.log('RecipeRouter connected...')
  })
app.get('/Alle', jsonParser, (req, res) => {
    let sql = `SELECT recipeingredients.RecipeID, products.ProductName, recipeingredients.Quantity, recipes.RecipeName FROM recipeingredients INNER JOIN products ON recipeingredients.ProductID = products.ProductID INNER JOIN recipes ON recipeingredients.RecipeID = recipes.RecipeID`;
    let query = db.query(sql, (err, result) => {
        if (err) {
            throw err;
        }
        else {
            console.log(result);
            res.json(result)
        }
        
    })
})

app.get('/Soup', jsonParser, (req, res) => {
  let sql = `SELECT recipeingredients.RecipeID, products.ProductName, recipeingredients.Quantity, recipes.RecipeName FROM recipeingredients INNER JOIN products ON recipeingredients.ProductID = products.ProductID INNER JOIN recipes ON recipeingredients.RecipeID = recipes.RecipeID WHERE recipes.RecipeID = 1`;
  let query = db.query(sql, (err, result) => {
      if (err) {
          throw err;
      }
      else {
          console.log(result);
          res.json(result)
      }
      
  })
})

app.get('/Gratin', jsonParser, (req, res) => {
  let sql = `SELECT recipeingredients.RecipeID, products.ProductName, recipeingredients.Quantity, recipes.RecipeName FROM recipeingredients INNER JOIN products ON recipeingredients.ProductID = products.ProductID INNER JOIN recipes ON recipeingredients.RecipeID = recipes.RecipeID WHERE recipes.RecipeID = 2`;
  let query = db.query(sql, (err, result) => {
      if (err) {
          throw err;
      }
      else {
          console.log(result);
          res.json(result)
      }
      
  })
})

app.get('/Pizza', jsonParser, (req, res) => {
  let sql = `SELECT recipeingredients.RecipeID, products.ProductName, recipeingredients.Quantity, recipes.RecipeName FROM recipeingredients INNER JOIN products ON recipeingredients.ProductID = products.ProductID INNER JOIN recipes ON recipeingredients.RecipeID = recipes.RecipeID WHERE recipes.RecipeID = 3`;
  let query = db.query(sql, (err, result) => {
      if (err) {
          throw err;
      }
      else {
          console.log(result);
          res.json(result)
      }
      
  })
})







module.exports = app;