-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 09. Jun 2020 um 13:23
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `nodemysql`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`ProductID`, `ProductName`) VALUES
(1, 'Cheese'),
(2, 'Corn'),
(3, 'Meat'),
(4, 'Onion'),
(5, 'Pea'),
(6, 'Oregano'),
(7, 'Potato'),
(8, 'Tomato');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recipeingredients`
--

CREATE TABLE `recipeingredients` (
  `RecipeID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `recipeingredients`
--

INSERT INTO `recipeingredients` (`RecipeID`, `ProductID`, `Quantity`) VALUES
(1, 7, 3),
(1, 4, 1),
(1, 2, 5),
(2, 3, 2),
(2, 4, 2),
(2, 5, 5),
(3, 1, 1),
(3, 8, 3),
(3, 6, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recipes`
--

CREATE TABLE `recipes` (
  `RecipeID` int(11) NOT NULL,
  `RecipeName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `recipes`
--

INSERT INTO `recipes` (`RecipeID`, `RecipeName`) VALUES
(1, 'Soup'),
(2, 'Gratin'),
(3, 'Pizza');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `storeranges`
--

CREATE TABLE `storeranges` (
  `StoreID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `storeranges`
--

INSERT INTO `storeranges` (`StoreID`, `ProductID`, `Price`) VALUES
(1, 1, 8),
(1, 2, 3),
(1, 3, 6),
(1, 4, 4),
(1, 5, 1),
(1, 6, 7),
(1, 7, 5),
(1, 8, 6),
(2, 1, 6),
(2, 2, 2),
(2, 3, 9),
(2, 4, 5),
(2, 5, 2),
(2, 6, 8),
(2, 7, 3),
(2, 8, 4),
(3, 1, 6),
(3, 2, 2),
(3, 3, 9),
(3, 4, 5),
(3, 5, 2),
(3, 6, 6),
(3, 7, 3),
(3, 8, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stores`
--

CREATE TABLE `stores` (
  `StoreID` int(11) NOT NULL,
  `StoreName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `stores`
--

INSERT INTO `stores` (`StoreID`, `StoreName`) VALUES
(1, 'Edoka'),
(2, 'Brutto'),
(3, 'Were');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indizes für die Tabelle `recipeingredients`
--
ALTER TABLE `recipeingredients`
  ADD KEY `RecipeID` (`RecipeID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Indizes für die Tabelle `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`RecipeID`);

--
-- Indizes für die Tabelle `storeranges`
--
ALTER TABLE `storeranges`
  ADD KEY `StoreID` (`StoreID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Indizes für die Tabelle `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`StoreID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `recipes`
--
ALTER TABLE `recipes`
  MODIFY `RecipeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `stores`
--
ALTER TABLE `stores`
  MODIFY `StoreID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `recipeingredients`
--
ALTER TABLE `recipeingredients`
  ADD CONSTRAINT `recipeingredients_ibfk_1` FOREIGN KEY (`RecipeID`) REFERENCES `recipes` (`RecipeID`),
  ADD CONSTRAINT `recipeingredients_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`);

--
-- Constraints der Tabelle `storeranges`
--
ALTER TABLE `storeranges`
  ADD CONSTRAINT `storeranges_ibfk_1` FOREIGN KEY (`StoreID`) REFERENCES `stores` (`StoreID`),
  ADD CONSTRAINT `storeranges_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
