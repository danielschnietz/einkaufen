const letters = /^[0-9a-zA-Z]+$/;
function getRecipes(recipe) {
    let Zutat;
    let Rezept;
    let Anzahl;

    $.get(`/recipes/${recipe}`, (data) => {
        $('.result').html('')
        let current;
        let i = -1;
        let total = 0;
        data.forEach((obj) => {
            Zutat = obj.ProductName;
            Rezept = obj.RecipeName;
            Anzahl = obj.Quantity;
            if (current != obj.RecipeID) {
                if (i >= 0) {
                    $(`.table${i}`).append(`<div>Anzahl aller benötigten Zutaten: ${total}</div> `)
                }
                total = 0;
                i++
                current = obj.RecipeID;
                $(".result").append(`<div class ="column table${i}"><table id = "table${i}" class = "table"><h2 class ="label is-normal">${Rezept}</h2><thead id = "thead${i}"><tr><th>Zutat</th><th>Anzahl</th></tr></thead><tbody id = "tbody${i}"></tbody></div`);
                $(`#tbody${i}`).append(`<tr><td>${Zutat}</td><td>${Anzahl}</td></tr>`);
                total += obj.Quantity;
            }
            else {
                $(`#tbody${i}`).append(`<tr><td>${Zutat}</td><td>${Anzahl}</td></tr>`);
                total += obj.Quantity;
            }
        })
        $(`.table${i}`).append(`<div class = "sumIngredients">Anzahl aller benötigten Zutaten: ${total}</div> `)
    })
}

let i = -1;
let storePrices = {};
let resultString = ''
let recipeArray = [];
let sumPrices = 0;
function getStoreRecipePrices(store, recipe, isAnArray, budget) {
    if (event.target.classList.contains('storeCost')) {
        $('.result').html('')
        storePrices = {}
        resultString = ''
        recipeArray = [];
        sumPrices = 0;
    }

    let Menge;
    let Preis;
    let Produkt;
    let datarecipe;
    let datastore;
    let total = 0;
    let recipeObj = {};
    let storeObj = {};
    if (isAnArray === true && i === -1) {
        $('.result').html('')
        storePrices = {}
        resultString = ''
        recipeArray = [];
        sumPrices = 0;
    }

    $.get(`/recipes/${recipe}`, (recipes) => {
        datarecipe = recipes
        datarecipe.forEach((obj) => {
            for (name in obj) {
                if (name == "ProductName") {
                    let save = obj[name]
                    recipeObj[save] = obj.Quantity;
                }
            }
        })

        $.get(`/stores/${store}`, (stores) => {
            datastore = stores;

            datastore.forEach((obj) => {
                for (name in obj) {
                    if (name == "ProductName") {
                        let save = obj[name]
                        storeObj[save] = obj.Price;
                    }
                }
            })

            if (budget) {
                i++
            }

            $(".result").append(`<div class = "column table${i}"><table id = "tablestore${i}" class = "table"><h2 class = "label is-normal">${recipe} bei ${store}</h2><thead id = "theadstore${i}"><tr><th>Produkt</th><th>Menge</th><th>Preis</th></tr></thead><tbody id = "tbodystore${i}"></tbody></div>`);
            for (let nameRec in recipeObj) {
                if (storeObj[nameRec]) {
                    Menge = recipeObj[nameRec]
                    Produkt = nameRec;
                    Preis = storeObj[nameRec];
                    total += recipeObj[nameRec] * storeObj[nameRec]
                    $(`#tbodystore${i}`).append(`<tr><td>${Produkt}</td><td>${Menge}</td><td>${Preis}</td></tr>`);
                }
                $(`#tbody${i}`).append(`<tr><td>${Produkt}</td><td>${Preis}</td></tr>`);

                $(`#tbody${i}`).append(`<tr><td>${Produkt}</td><td>${Preis}</td></tr>`);
            }
            $(`.table${i}`).append(`<div>Preis für das Rezept ${recipe} bei ${store}: ${total}</div> `)
            storePrices[recipe] = total
            if (i === 2) {
                for (key in storePrices) {
                    sumPrices = storePrices[key]
                    if (sumPrices <= budget) {
                        recipeArray.push(key)
                    }
                }
                if (Array.isArray(recipeArray) && recipeArray.length) {
                    resultString = recipeArray.join(', ')
                    $(".result").append(`<div class = "column is-2"><h2 class ="subtitle"> ${resultString} bei ${store} für ein Budget von ${budget}</h2></div> `)
                }
                else {

                    $(".result").append(`<div class = "column is-2"><h2 class = "subtitle">Keine Rezepte für ein Budget von ${budget} gefunden</h2></div> `)
                }
            }
        })
    })
}

function getCheapestStore(store, recipe) {
    $(".result").html('')

    let datarecipe;
    let datastore;
    let total = 0;
    let recipeObj = {};
    let storeObj = {};

    $.get(`/recipes/${recipe}`, (recipes) => {
        datarecipe = recipes
        datarecipe.forEach((obj) => {
            for (name in obj) {
                if (name == "ProductName") {
                    let save = obj[name]
                    recipeObj[save] = obj.Quantity;
                }
            }
        })
        $.get(`/stores/${store}`, (stores) => {
            datastore = stores;
            datastore.forEach((obj) => {
                for (name in obj) {
                    if (name == "ProductName") {
                        let save = obj[name]
                        storeObj[save] = obj.Price;
                    }
                }
            })
            let cheapestPrice = Number.MAX_VALUE;
            let cheapestName;
            let currentName;
            $(".result").html();
            for (test of datastore) {
                if (total === 0) {
                    currentName = test.StoreName
                }
                for (ing in recipeObj) {
                    if (test.ProductName === ing) {
                        if (test.StoreName != currentName) {
                            if (total < cheapestPrice) {
                                cheapestPrice = total;
                                cheapestName = currentName;
                                total = 0;
                                currentName = test.StoreName
                                total += recipeObj[ing] * test.Price
                            }
                            else {
                                total = 0;
                                currentName = test.StoreName
                                total += recipeObj[ing] * test.Price
                            }
                        }
                        else {
                            total += recipeObj[ing] * test.Price
                        }
                    }
                }
            }
            $(".result").append(`<div class = "has-text-centered"><h2 class = "subtitle">${recipe} ist am günstigsten bei ${cheapestName} für ${cheapestPrice}</h2></div> `)
        })
    })
}

let totalAmounts = {};
let storedRecipes = [];

function getShoppingList(recipes) {
    $(".result").html('');
    console.log(recipes)
    let count = 0;
    let i = 0;
    let length = recipes.length;
    let recipeList = {};
    recipes.forEach((recipe) => {

        $.get(`/recipes/${recipe}`, (recipes) => {
            recipes.forEach((recipeRes) => {
                for (name in recipeRes) {
                    if (name == "ProductName") {
                        let save = recipeRes[name]
                        if (recipeList[save]) {
                            recipeList[save] += recipeRes.Quantity;
                        }
                        else {
                            recipeList[save] = recipeRes.Quantity;
                        }
                    }
                }
            })
            count++;
            if (count === length) {
                for (prop in recipeList) {
                    Zutat = prop;
                    Anzahl = recipeList[prop];
                    if (i === 0) {
                        i++;
                        $(".result").append(`<div class = "column is-2"><table id = "table${i}" class = "table"><thead id = "thead${i}"><tr><th>Zutat</th><th>Anzahl</th></tr></thead><tbody id = "tbody${i}"></tbody></div>`);
                        $(`#tbody${i}`).append(`<tr><td>${Zutat}</td><td>${Anzahl}</td></tr>`);
                    }
                    else {
                        $(`#tbody${i}`).append(`<tr><td>${Zutat}</td><td>${Anzahl}</td></tr>`);
                    }
                }
            }
        })
    })
}

function getRecipesForBudget(budget, selectedStore) {
    let isAnArray = false;
    let count = 0;
    let recipeArray = ['Soup', 'Gratin', 'Pizza'];
    recipeArray.forEach((recipeNumber) => {
        if (count === 0) {
            i = -1
            isAnArray = true;
        }
        else {
            isAnArray = false;
        }
        getStoreRecipePrices(selectedStore, recipeNumber, isAnArray, budget)
        count++
    })
}
$.ajaxSetup({
    contentType: "application/json; charset=utf-8"
});


function signUp(username, vorname, nachname, password) {
    if (username.length == 0 || password.length == 0) {
        return
    }

    else if (username.match(letters) && password.match(letters)) {
        let data = new Object();
        data.username = username;
        data.vorname = vorname;
        data.nachname = nachname;
        data.password = password;
        data = JSON.stringify(data)
        $.post(`/login/signup/checkName`, data, (response) => {
            nameCheck = response
            if (nameCheck.startsWith('valid')) {
                $.post(`/login/signup`, data, (response) => {
                    if (response === 'true') {
                        $('#regform').addClass('is-hidden')
                        $('#signupres').removeClass('is-hidden')
                    }
                })
            }
        })
    }
}

function login(username, password) {
    if (username.length == 0 || password.length == 0) {
        return
    }
    else if (username.match(letters) && password.match(letters)) {
        let data = new Object();
        data.name = username;
        data.password = password
        data = JSON.stringify(data)
        $.post(`/login/login`, data, (response) => {
            $('passwordLoginCheck').html('').removeClass('is-danger')
            console.log(response)
            if (response === 'true') {
                window.location.replace("einkaufen.html");
            }
            else {
                $('#passwordLoginCheck').html('Passwort oder Username falsch').addClass('is-danger')
            }
        })
    }
    else {
        return
    }
}

function addHidden() {
    $('.result').html('')
    let dataProp = $(`[data-prop = ${this.event.target.id}]`)
    $('.beHidden').addClass("is-hidden")
    $(dataProp).removeClass("is-hidden")

}

function checkLengthAndIfUsernameExists(username) {


    let nameCheck;

    if (username.length < 5) {
        $('#usernameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
        $('#usernameCheckExists').html('Username benötigt mindestens 5 Zeichen').addClass('is-danger')
        return;
    }
    else if (username.match(letters)) {
        let data = new Object();
        data.name = username;
        data = JSON.stringify(data)
        $.post(`/login/signup/checkName`, data, (response) => {
            $('#usernameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
            nameCheck = response
            console.log(nameCheck)
            if (nameCheck.startsWith('valid')) {

                $('#usernameCheckExists').html(nameCheck).addClass("is-success")
                return true;
            }
            else {
                $('#usernameCheckExists').html(nameCheck).addClass("is-danger")
            }
        })
        return false
    }
    else {
        $('#usernameCheckExists').html('Username enthält ungültige Zeichen').addClass("is-danger")
        return
    }

}
function checkFirstname(vorname) {
    if (vorname.match(letters)) {
        $('#firstnameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
    }
    else {

        $('#firstnameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
        $('#firstnameCheckExists').html('Vorname enthält ungültige Zeichen').addClass('is-danger')
        return;
    }
}

function checkName(nachname) {
    if (nachname.match(letters)) {
        $('#lastnameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
    }
    else {

        $('#lastnameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
        $('#lastnameCheckExists').html('Nachname enthält ungültige Zeichen').addClass('is-danger')
        return;
    }
}

function checkPasswordCharacters(password) {
    if (password.match(letters)) {
        $('#lastnameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
    }
    else {

        $('#lastnameCheckExists').html('').removeClass('is-success').removeClass('is-danger')
        $('#lastnameCheckExists').html('Passwort enthält ungültige Zeichen').addClass('is-danger')
        return;
    }
}

let store;
let recipe;
$(document).ready(() => {
    $('.submit').click(() => {
        recipe = $('#recipes option:selected').text()
        getRecipes(recipe);
    })
    $('.storeCost').click(() => {
        store = $('#stores option:selected').text();
        recipe = $('#storeRecipe option:selected').text();
        getStoreRecipePrices(store, recipe);
    });
    $('.cheapestStore').click(() => {
        store = 'Alle';
        recipe = $('#cheapestStoreRecipeStore option:selected').text();
        getCheapestStore(store, recipe);
    });

    $('.shoppingList').click(() => {
        let recipes = [];
        let checked = $(".check:checked")
        for (box of checked) {
            let recipe = (box.name)
            recipes.push(recipe)
        }
        getShoppingList(recipes);
    })
    $('.budgetButton').click(() => {
        let budget = $('.input').val()
        let selectedStore = $('#storesForPossibleRecipes option:selected').text();
        getRecipesForBudget(budget, selectedStore);
    })
    $('#signup').click(() => {
        username = $('#username').val()
        vorname = $('#firstnameSignIn').val()
        nachname = $('#lastnameSignIn').val()
        password = $('#passwordSignIn').val()
        signUp(username, vorname, nachname, password);
    })
    $('#loginButton').click(() => {
        username = $('#usernameLogin').val()
        password = $('#passwordLogin').val()
        login(username, password);
    })

    $('#reg').click(() => {
        window.location.replace("signup.html");
        $('#regform').removeClass('is-hidden')
        $('$signupres').addClass('is-hidden')
    })
    $('#login').click(() => {
        window.location.replace("login.html");
    })
    $('#home').click(() => {
        window.location.replace("start.html");
    })
    $('#logout').click(() => {
        window.location.replace("start.html");
    })
    $('.navbutton').click(() => {
        addHidden();
    })
    $('#username').blur(() => {
        let username = $('#username').val()
        checkLengthAndIfUsernameExists(username)
    })
    $('#lastnameSignIn').blur(() => {
        let nachname = $('#lastnameSignIn').val()
        checkName(nachname)
    })
    $('#firstnameSignIn').blur(() => {
        let vorname = $('#firstnameSignIn').val()
        checkFirstname(vorname)
    })
    $('#passwordSignIn').blur(() => {
        let password = $('#passwordSignIn').val()
        checkPasswordCharacters(password)
    })
    $('#signupresbutton').click(() => {
        window.location.replace("login.html");
    })

})


